//
//  PodcastEpisode.swift
//  Podcast Experience
//
//  Created by August Freytag on 25/11/2017.
//  Copyright © 2017 August S. Freytag. All rights reserved.
//

import Cocoa

struct PodcastEpisode {
	let title: String?
	let publication: Date?
	let synopsis: String?
	let audioURL: URL?
}
