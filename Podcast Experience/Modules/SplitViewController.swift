//
//  SplitViewController.swift
//  Podcast Experience
//
//  Created by August Freytag on 25/11/2017.
//  Copyright © 2017 August S. Freytag. All rights reserved.
//

import Cocoa

class SplitViewController: NSSplitViewController {

	@IBOutlet weak var podcastsViewItem: NSSplitViewItem!
	@IBOutlet weak var selectionViewItem: NSSplitViewItem!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		setupOverpass()
    }
	
	func setupOverpass() {
		if
			let podcastsViewController = podcastsViewItem.viewController as? PodcastsViewController,
			let selectionViewController = selectionViewItem.viewController as? PodcastSelectionViewController {
			podcastsViewController.podcastSelectionViewController = selectionViewController
			selectionViewController.podcastsViewController = podcastsViewController
		}
	}
    
}
