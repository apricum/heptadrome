//
//  PodcastsRetriever.swift
//  Podcast Experience
//
//  Created by August Freytag on 19/11/2017.
//  Copyright © 2017 August S. Freytag. All rights reserved.
//

import Cocoa

class PodcastRetriever {
	
	let observerNames : [String: NSNotification.Name] = [
		"podcastsLoaded": NSNotification.Name("podcastsLoaded"),
		"podcastEpisodesLoaded": NSNotification.Name("podcastEpisodesLoaded")
	]
	
	var cachedPodcastEpisodes : [String: [PodcastEpisode]] = [:]
	
	func fetchAndProcessPodcast(context: NSManagedObjectContext, from: URL) {
		let securedFeedURL = getHTTPSPrefixedURL(from)
		getFeedIndexer(feedURL: securedFeedURL) { (_ xmlIndexer: XMLIndexer) in
			// Handle podcast series data.
			let podcastData = self.getPodcastData(from: xmlIndexer)
			let podcast : Podcast = self.getPodcast(context: context, withData: podcastData)
			print("Podcast series constructed with title '\(podcast.title ?? "<no title>")' from stored feed url '\(podcast.feedURL ?? "<no feed url>")'.")
			(NSApplication.shared.delegate as? AppDelegate)?.saveAction(nil)
			NotificationCenter.default.post(name: self.observerNames["podcastsLoaded"]!, object: nil, userInfo: ["objectId": podcast.objectID])
		}
	}
	
	func fetchAndProcessPodcastEpisodes(from: Podcast) {
		let podcast = from
		// Determine podcast feed URL from data model.
		let securedFeedURL = URL(string: podcast.feedURL ?? "")
		if securedFeedURL == nil {
			return
		}
		print("Fetching podcast episodes from stored feed '\(securedFeedURL!)'.")
		// Fetch all episodes from podcast series' feed URL.
		getFeedIndexer(feedURL: securedFeedURL!) { (_ xmlIndexer: XMLIndexer) in
			// Handle individual podcast episodes data.
			let podcastEpisodesData = self.getPodcastEpisodesData(from: xmlIndexer)
			var podcastEpisodes : [PodcastEpisode] = []
			for podcastEpisodeData in podcastEpisodesData {
				let podcastEpisode = self.getPodcastEpisode(withData: podcastEpisodeData)
				podcastEpisodes.append(podcastEpisode)
			}
			self.cachedPodcastEpisodes[podcast.feedURL!] = podcastEpisodes
			NotificationCenter.default.post(name: self.observerNames["podcastEpisodesLoaded"]!, object: nil, userInfo: ["feedURL": podcast.feedURL as Any])
		}
	}
	
	func getHTTPSPrefixedURL(_ from: URL) -> URL {
		if from.absoluteString.hasPrefix("https://") {
			return from
		}
		let inputIsolatedURLAbsolute : String = getIsolatedURL(from).absoluteString
		return URL(string: "https://" + inputIsolatedURLAbsolute)!
	}
	
	func getIsolatedURL(_ from: URL) -> URL {
		let urlAbsolute = from.absoluteString
		let urlIsolatedAbsolute = urlAbsolute
			.replacingOccurrences(of: "http://", with: "")
			.replacingOccurrences(of: "https://", with: "")
		return URL(string: urlIsolatedAbsolute)!
	}
	
	func getFeedIndexer(feedURL: URL, completionHandler: @escaping (_ xmlIndexer: XMLIndexer) -> Void) {
		let securedFeedURL = getHTTPSPrefixedURL(feedURL)
		let feedDataTask = URLSession.shared.dataTask(with: securedFeedURL) { (data: Data?, response: URLResponse?, error: Error?) in
			if error != nil {
				print(error!.localizedDescription)
				return
			}
			if data == nil {
				print("Expected podcast feed from \"\(securedFeedURL)\" as data, found none.")
				return
			}
			let xmlIndexer : XMLIndexer = self.parseFeedContents(feedContents: data!)
			completionHandler(xmlIndexer)
		}
		feedDataTask.resume()
	}
	
	func parseFeedContents(feedContents: Data) -> XMLIndexer {
		let podcastFeedParser = PodcastFeedParser()
		let podcastFeedXMLIndexer = podcastFeedParser.parse(feedContents: feedContents)
		return podcastFeedXMLIndexer
	}
	
	func getPodcastEpisodes(forSeries: Podcast) -> [PodcastEpisode] {
		let podcastFeedURL = forSeries.feedURL
		if podcastFeedURL == nil || cachedPodcastEpisodes[podcastFeedURL!] == nil {
			return []
		}
		return cachedPodcastEpisodes[podcastFeedURL!]!;
	}
	
	func getPodcast(context: NSManagedObjectContext, withData: (title: String?, feedURL: String?, portraitURL: String?)) -> Podcast {
		let podcast = Podcast(context: context)
		podcast.title = withData.title
		podcast.feedURL = withData.feedURL
		podcast.portraitURL = withData.portraitURL
		return podcast;
	}
	
	func getPodcastData(from: XMLIndexer) -> (title: String?, feedURL: String?, portraitURL: String?) {
		let xmlIndexer = from
		return (
			xmlIndexer["rss"]["channel"]["title"].element?.text,
			xmlIndexer["rss"]["channel"]["atom:link"][0].element?.attribute(by: "href")?.text,
			xmlIndexer["rss"]["channel"]["image"]["url"].element?.text
		)
	}
	
	func getPodcastEpisode(withData: (title: String?, publication: Date?, synopsis: String?, audioURL: URL?)) -> PodcastEpisode {
		let podcastEpisode = PodcastEpisode(
			title: withData.title,
			publication: withData.publication,
			synopsis: withData.synopsis,
			audioURL: withData.audioURL
		)
		return podcastEpisode
	}
	
	func getPodcastEpisodesData(from: XMLIndexer) -> [(title: String?, publication: Date?, synopsis: String?, audioURL: URL?)] {
		var episodesData : [(title: String?, publication: Date?, synopsis: String?, audioURL: URL?)] = []
		let xmlIndexer = from
		for itemElement in xmlIndexer["rss"]["channel"]["item"].all {
			let itemTitle = itemElement["title"].element?.text
			let itemPublication = getPodcastDateFormatter().date(from: itemElement["pubDate"].element?.text ?? "")
			let itemSynopsis = itemElement["description"].element?.text
			let itemAudioURLString : String? = try? itemElement["enclosure"].withAttribute("type", "audio/mp4").element!.attribute(by: "url")!.text
			if itemAudioURLString == nil {
				print("Expected podcast episode '\(itemTitle ?? "<Untitled>")' to have audio stream, found none for 'audio/mp4'.")
			}
			let itemAudioURL : URL? = URL(string: itemAudioURLString ?? "")
			// Append episode data tuple to list.
			let episodeData = (title: itemTitle, publication: itemPublication, synopsis: itemSynopsis, audioURL: itemAudioURL)
			episodesData.append(episodeData)
		}
		return episodesData
	}
	
	func getPodcasts(context: NSManagedObjectContext) throws -> [Podcast] {
		// Prepare fetch request to retrieve all podcasts by title.
		let podcastFetchRequest = Podcast.fetchRequest() as NSFetchRequest<Podcast>
		podcastFetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
		// Commit fetch through view context to retrieve Podcast models.
		var podcasts : [Podcast] = []
		do {
			try podcasts = context.fetch(podcastFetchRequest)
		} catch(let exception) {
			throw exception
		}
		return podcasts
	}
	
	func clearPodcasts(context: NSManagedObjectContext) -> Bool {
		let fetchRequest = Podcast.fetchRequest() as NSFetchRequest<NSFetchRequestResult>
		let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
		do {
			try context.execute(batchDeleteRequest)
		} catch(let exception) {
			print("Failed to remove persistent podcasts, got '\(exception.localizedDescription)'.")
			return false
		}
		return true
	}
	
	func getPodcastDateFormatter() -> DateFormatter {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "ccc, dd MMM y HH:mm:ss xx"
		return dateFormatter
	}

}

