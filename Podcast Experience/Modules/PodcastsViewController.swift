//
//  PodcastsViewController.swift
//  Podcast Experience
//
//  Created by August Freytag on 19/11/2017.
//  Copyright © 2017 August S. Freytag. All rights reserved.
//

import Cocoa

class PodcastsViewController : NSViewController, NSTableViewDataSource, NSTableViewDelegate {

	@IBOutlet weak var podcastURLInput: NSTextField!
	@IBOutlet weak var podcastAddInput: NSButton!
	@IBOutlet weak var podcastSeriesTableView: NSTableView!
	
	// Instance & Reference Stack
	var podcastSelectionViewController : PodcastSelectionViewController? = nil
	let podcastRetriever = PodcastRetriever()
	// Preferences
	let observedNames : [String: NSNotification.Name] = [
		"podcastsLoaded": NSNotification.Name("podcastsLoaded"),
		"podcastEpisodesLoaded": NSNotification.Name("podcastEpisodesLoaded")
	]
	// Data source properties
	var podcasts : [Podcast] = []
	
    override func viewDidLoad() {
        super.viewDidLoad()
		initObservers()
		// clearPodcasts()
		getPodcasts()
    }
	
	@IBAction func podcastAddInputClicked(_ sender: Any) {
		let podcastFeedURL = URL(string: podcastURLInput.stringValue)
		if podcastFeedURL == nil {
			return
		}
		let viewContext = getViewContext()
		podcastRetriever.fetchAndProcessPodcast(context: viewContext, from: podcastFeedURL!)
	}
	
	func initObservers() {
		NotificationCenter.default.addObserver(
			forName: observedNames["podcastsLoaded"]!,
			object: nil,
			queue: OperationQueue.main
		) { notification in
			print("Received notification for podcasts loaded.")
			self.getPodcasts()
		}
		NotificationCenter.default.addObserver(
			forName: observedNames["podcastEpisodesLoaded"]!,
			object: nil,
			queue: OperationQueue.main
		) { notification in
			let payload = notification.userInfo as? [String: AnyObject?]
			let currentlySelectedPodcast = self.podcastSelectionViewController?.selectedPodcast
			let selectedPodcastFeedURL : String? = payload?["feedURL"] as? String
			if currentlySelectedPodcast?.feedURL != selectedPodcastFeedURL {
				return
			}
			self.getPodcastEpisodes()
			self.podcastSelectionViewController?.updateEpisodes()
		}
	}
	
	func getPodcasts() {
		let viewContext = getViewContext()
		podcasts = try! podcastRetriever.getPodcasts(context: viewContext)
		reloadTableView()
	}
	
	func getPodcastEpisodes() {
		let currentlySelectedPodcast = self.podcastSelectionViewController?.selectedPodcast
		if currentlySelectedPodcast == nil {
			return
		}
		let selectedPodcastEpisodes : [PodcastEpisode] = self.podcastRetriever.getPodcastEpisodes(forSeries: currentlySelectedPodcast!)
		self.podcastSelectionViewController?.selectedPodcastEpisodes = selectedPodcastEpisodes
	}
	
	func clearPodcasts() {
		let viewContext = getViewContext()
		if podcastRetriever.clearPodcasts(context: viewContext) == false {
			print("Podcasts could not be cleared from persistent store.")
		}
	}
	
	func reloadTableView() {
		podcastSeriesTableView.reloadData()
	}
	
	func numberOfRows(in tableView: NSTableView) -> Int {
		return podcasts.count
	}
	
	func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
		let podcast = podcasts[row]
		
		let tableViewCell = tableView.makeView(
				withIdentifier: NSUserInterfaceItemIdentifier("podcastSeriesCellView"),
				owner: self
			) as? NSTableCellView
		
		tableViewCell?.textField?.stringValue = podcast.title ?? "<Untitled Series>"
		
		return tableViewCell
	}
	
	func tableViewSelectionDidChange(_ notification: Notification) {
		let selectedRow = podcastSeriesTableView.selectedRow
		let selectedPodcast : Podcast? = selectedRow != -1 ? podcasts[selectedRow] : nil
		// Fetch podcast episodes or serve from cache, send notification when ready.
		if selectedPodcast != nil {
			podcastRetriever.fetchAndProcessPodcastEpisodes(from: selectedPodcast!)
		}
		// Pass selection choice to responsible view controller to display details.
		podcastSelectionViewController?.selectedPodcast = selectedPodcast
		podcastSelectionViewController?.updateSelectedPodcastView()
		
	}

	func getViewContext() -> NSManagedObjectContext {
		return (NSApplication.shared.delegate as? AppDelegate)!.persistentContainer.viewContext
	}
}










