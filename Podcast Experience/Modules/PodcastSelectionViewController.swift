//
//  PodcastsEpisodesViewController.swift
//  Podcast Experience
//
//  Created by August Freytag on 25/11/2017.
//  Copyright © 2017 August S. Freytag. All rights reserved.
//

import Cocoa
import AVKit

class PodcastSelectionViewController: NSViewController, NSTableViewDataSource, NSTableViewDelegate {

	// Outlets: Labels & Displays
	@IBOutlet weak var podcastTitleLabel: NSTextField!
	@IBOutlet weak var podcastEpisodeTitleLabel: NSTextField!
	@IBOutlet weak var podcastPortraitImageView: NSImageView!
	@IBOutlet weak var selectedPodcastEpisodesTableView: NSTableView!
	
	// Outlets: Buttons
	@IBOutlet weak var podcastPlaybackButton: NSButton!
	@IBOutlet weak var podcastRemoveSelectedButton: NSButton!

	var player : AVPlayer? = nil
	var podcastsViewController : PodcastsViewController? = nil
	var selectedPodcast : Podcast? = nil
	var selectedPodcastEpisodes : [PodcastEpisode] = []

	override func viewDidLoad() {
		super.viewDidLoad()
		updateSelectedPodcastView()
	}
	
	@IBAction func podcastPlaybackButtonClicked(_ sender: Any) {
	}

	@IBAction func podcastRemoveSelectedButtonClicked(_ sender: Any) {
		if selectedPodcast != nil {
			removeSelectedPodcast()
		}
	}
	
	func updateSelectedPodcastView() {
		updateInputStates()
		updateLabels()
	}
	
	func updateEpisodes() {
		selectedPodcastEpisodesTableView.reloadData()
	}
	
	func updateInputStates() {
		if selectedPodcast == nil {
			podcastPlaybackButton.isEnabled = false
			podcastRemoveSelectedButton.isEnabled = false
		} else {
			podcastPlaybackButton.isEnabled = true
			podcastRemoveSelectedButton.isEnabled = true
		}
	}
	
	func updateLabels() {
		// Title Label
		podcastTitleLabel.stringValue = selectedPodcast?.title ?? "No podcast selected."
		// Portrait Image
		if selectedPodcast?.portraitURL != nil {
			let portraitURL : URL? = URL(string: selectedPodcast?.portraitURL! ?? "")
			let image = NSImage(byReferencing: portraitURL!)
			podcastPortraitImageView.image = image
		} else {
			podcastPortraitImageView.image = nil
		}
	}
	
	func removeSelectedPodcast() {
		let appDelegate : AppDelegate? = NSApplication.shared.delegate as? AppDelegate
		let viewContext = appDelegate?.persistentContainer.viewContext
		viewContext?.delete(selectedPodcast!)
		appDelegate?.saveAction(nil)
		podcastsViewController?.getPodcasts()
		selectedPodcast = nil
		updateSelectedPodcastView()
		
	}

	func numberOfRows(in tableView: NSTableView) -> Int {
		return selectedPodcastEpisodes.count
	}
	
	func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
		let selectedPodcastEpisode = selectedPodcastEpisodes[row]
		let tableCellView = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier("selectedPodcastEpisodeCell"), owner: self) as? NSTableCellView
		tableCellView?.textField?.stringValue = selectedPodcastEpisode.title ?? "<Untitled Episode>"
		return tableCellView
	}
	
	func tableViewSelectionDidChange(_ notification: Notification) {
		let selectedRow = selectedPodcastEpisodesTableView.selectedRow
		if selectedRow == -1 {
			return
		}
		if selectedRow >= selectedPodcastEpisodes.count {
			print("Expected podcast episode for selected index \(selectedRow) not found, range exceeded.")
			return
		}
		let podcastEpisode : PodcastEpisode = selectedPodcastEpisodes[selectedRow]
		if podcastEpisode.audioURL == nil {
			print("Expected audio URL for selected podcast episode #\(selectedRow) titled '\(podcastEpisode.title ?? "<Untitled>")', found none.")
			return
		}
		let playerAudioURL = podcastEpisode.audioURL!
		print("Starting playback for '\(playerAudioURL)'.")
		player = AVPlayer(url: playerAudioURL)
		player!.play()
	}
}





