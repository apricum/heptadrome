//
//  PodcastFeedParser.swift
//  Podcast Experience
//
//  Created by August Freytag on 19/11/2017.
//  Copyright © 2017 August S. Freytag. All rights reserved.
//

import Foundation

class PodcastFeedParser {
	
	func parse(feedContents: Data) -> XMLIndexer {
		print("Preparing to parse received feed contents, consisting of \(feedContents.count) element(s).")
		let xmlContents = SWXMLHash.parse(feedContents)
		return xmlContents
	}
	
}
